package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static fr.umfds.exo2.Couleur.PIQUE;
import static fr.umfds.exo2.Couleur.TREFLE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JeuDeCartesTest {

    @Mock
    private Random rand;

    @InjectMocks
    private JeuDeCartes jeuDeCartes;

    @Test
    public void pickLastCardTest() throws CarteIncorrecteException {
        when(rand.nextInt(anyInt())).thenReturn(51);
        assertEquals(jeuDeCartes.pioche(), new Carte(13, TREFLE));
    }

    @Test
    public void pickFirstCardTest() throws CarteIncorrecteException {
        when(rand.nextInt(anyInt())).thenReturn(0);
        assertEquals(jeuDeCartes.pioche(), new Carte(1, PIQUE));
    }

    @Test
    public void distributeTest() throws CarteIncorrecteException, Exception {
        when(rand.nextInt(anyInt())).thenReturn(0, 0, 0, 48, 47, 46);
        final var paquets = jeuDeCartes.distribution(3, 2);
        assertEquals(paquets.size(), 2);
        // Ici on vérifie juste si les éléments sont égaux.
        assertEquals(new ArrayList<>(paquets.get(0)), List.of(
                new Carte(1, PIQUE), new Carte(2, PIQUE), new Carte(3, PIQUE)
        ));
        assertEquals(new ArrayList<>(paquets.get(1)), List.of(
                new Carte(13, TREFLE), new Carte(12, TREFLE), new Carte(11, TREFLE)
        ));
    }

}
