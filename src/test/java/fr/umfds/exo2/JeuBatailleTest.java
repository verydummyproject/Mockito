package fr.umfds.exo2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import static fr.umfds.exo2.Couleur.COEUR;
import static fr.umfds.exo2.Couleur.TREFLE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JeuBatailleTest {

    @Mock
    private JeuDeCartes jeuDeCartes;

    @Test
    public void noBataille() throws Exception, CarteIncorrecteException {
        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(new ArrayList<>(List.of(
                new ArrayDeque<>(List.of(new Carte(1, TREFLE), new Carte(5, TREFLE))),
                new ArrayDeque<>(List.of(new Carte(2, COEUR), new Carte(3, COEUR)))
        )));
        final var bataille = new JeuBataille(jeuDeCartes);
        assertFalse(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), -1);
        assertFalse(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), -1);
        assertFalse(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), -1);
        assertTrue(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), 1);
    }

    @Test
    public void batailleOnce() throws Exception, CarteIncorrecteException {
        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(new ArrayList<>(List.of(
                new ArrayDeque<>(List.of(new Carte(1, TREFLE), new Carte(5, TREFLE))),
                new ArrayDeque<>(List.of(new Carte(1, COEUR), new Carte(3, COEUR)))
        )));
        final var bataille = new JeuBataille(jeuDeCartes);
        assertTrue(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), 1);
    }

    @Test
    public void batailleTwice() throws Exception, CarteIncorrecteException {
        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(new ArrayList<>(List.of(
                new ArrayDeque<>(List.of(new Carte(1, TREFLE), new Carte(3, TREFLE), new Carte(5, TREFLE))),
                new ArrayDeque<>(List.of(new Carte(1, COEUR), new Carte(3, COEUR), new Carte(13, COEUR)))
        )));
        final var bataille = new JeuBataille(jeuDeCartes);
        assertTrue(bataille.round());
        assertEquals(bataille.numJoueurGagnant(), 2);
    }

    @Test
    public void batailleEnd() throws Exception, CarteIncorrecteException {
        when(jeuDeCartes.distribution(anyInt(), anyInt())).thenReturn(new ArrayList<>(List.of(
                new ArrayDeque<>(List.of(new Carte(1, TREFLE), new Carte(3, TREFLE), new Carte(5, TREFLE))),
                new ArrayDeque<>(List.of(new Carte(1, COEUR), new Carte(3, COEUR), new Carte(5, COEUR)))
        )));
        final var bataille = new JeuBataille(jeuDeCartes);
        assertTrue(bataille.round());
        assertEquals(bataille.tailleJeuJoueur1(), 0);
        assertEquals(bataille.tailleJeuJoueur2(), 6);
    }

}
