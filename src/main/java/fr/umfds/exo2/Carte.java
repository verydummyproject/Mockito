package fr.umfds.exo2;

import java.util.Objects;

public class Carte {
    private final int hauteur;
    private final Couleur couleur;

    public Carte(int hauteur, Couleur couleur) throws CarteIncorrecteException {
        if (hauteur > 0 && hauteur < 14) {
            this.hauteur = hauteur;
        } else {
            throw new CarteIncorrecteException();
        }
        this.couleur = couleur;
    }

    public int hauteur() {
        return hauteur;
    }

    public Couleur couleur() {
        return couleur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carte carte = (Carte) o;
        return hauteur == carte.hauteur && couleur == carte.couleur;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hauteur, couleur);
    }

    @Override
    public String toString() {
        return "Carte { hauteur = " + hauteur + ", couleur = " + couleur + " }";
    }
}
