package fr.umfds.exo1;

public class ClassUnderTest {
    private final UsedClass uc;

    public ClassUnderTest(UsedClass uc) {
        this.uc = uc;
    }


    public long m1() {
        long uct;
        try {
            uct = uc.treatment1();
        } catch (TreatmentException e) {
            return 0;
        }
        return uct + 2;
    }

    public int m2() {
        return uc.treatment2() ? 1 : 0;
    }

}
